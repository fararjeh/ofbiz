package org.apache.ofbiz.souq.shipping.model;

public class FacilityMap {
	public static final String TABLE_NAME = "SouqFacilityMap";
	public static final String REL_CONNECTED_FACILITY = "connectedFacility" + Facility.TABLE_NAME;
	public static final String REL_CENTRAL_FACILITY = "centralFacility" + Facility.TABLE_NAME;

	public static final String CENTRAL_FACILITY_ID = "centralFacilityId";
	public static final String CONNECTED_FACILITY_ID = "connectedFacilityId";

	private Facility centralFacilityId;
	private Facility connectedFacilityId;

	public FacilityMap() {

	}

	public FacilityMap(Facility centralFacilityId, Facility connectedFacilityId) {
		super();
		this.centralFacilityId = centralFacilityId;
		this.connectedFacilityId = connectedFacilityId;
	}

	public Facility getCentralFacility() {
		return centralFacilityId;
	}

	public void setCentralFacility(Facility centralFacilityId) {
		this.centralFacilityId = centralFacilityId;
	}

	public Facility getConnectedFacility() {
		return connectedFacilityId;
	}

	public void setConnectedFacility(Facility connectedFacilityId) {
		this.connectedFacilityId = connectedFacilityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "FacilityMap [centralFacilityId=" + centralFacilityId + ", connectedFacilityId=" + connectedFacilityId + "]";
	}

}
