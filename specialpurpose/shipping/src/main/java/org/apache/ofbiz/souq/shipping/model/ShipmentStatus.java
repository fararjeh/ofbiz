package org.apache.ofbiz.souq.shipping.model;

public class ShipmentStatus {

	public static final String TABLE_NAME = "SouqShipmentStatus";
	public static final String SHIPMENT_STATUS_ID = "shipmentStatusId";
	public static final String STATUS_NAME = "statusName";

	public final static String CREATED = "CREATED";
	public final static String PICKED = "PICKED";
	public final static String MOVED = "MOVED";
	public final static String DELIVERED = "DELIVERED";

	private String shipmentStatusId;
	private String statusName;

	/**
	 * @return the shipmentStatusId
	 */
	public String getShipmentStatusId() {
		return shipmentStatusId;
	}

	/**
	 * @param shipmentStatusId
	 *            the shipmentStatusId to set
	 */
	public void setShipmentStatusId(String shipmentStatusId) {
		this.shipmentStatusId = shipmentStatusId;
	}

	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShipmentStatus [shipmentStatusId=" + shipmentStatusId + ", statusName=" + statusName + "]";
	}

}
