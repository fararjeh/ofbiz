package org.apache.ofbiz.souq.shipping;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilDateTime;
import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericEntityException;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.entity.condition.EntityCondition;
import org.apache.ofbiz.entity.condition.EntityOperator;
import org.apache.ofbiz.entity.transaction.GenericTransactionException;
import org.apache.ofbiz.entity.transaction.TransactionUtil;
import org.apache.ofbiz.entity.util.EntityQuery;
import org.apache.ofbiz.souq.shipping.model.City;
import org.apache.ofbiz.souq.shipping.model.Facility;
import org.apache.ofbiz.souq.shipping.model.FacilityMap;
import org.apache.ofbiz.souq.shipping.model.RouteCache;
import org.apache.ofbiz.souq.shipping.model.Shipment;
import org.apache.ofbiz.souq.shipping.model.ShipmentRoute;
import org.apache.ofbiz.souq.shipping.model.ShipmentStatus;

public class ShippingRepository {
	private static ShippingRepository shipmentRepository = null;
	public static final String module = ShippingRepository.class.getName();

	private ShippingRepository() {
	}

	/*
	 * Get single instance 
	 */
	public static ShippingRepository getInstance() {
		if (shipmentRepository == null) {
			synchronized (ShippingRepository.class) {
				if (shipmentRepository == null) {
					shipmentRepository = new ShippingRepository();
				}
			}
		}
		return shipmentRepository;
	}

	public String createShipment(Delegator delegator, Shipment shipment, GenericValue currentUserName) throws ShippingException {
		boolean beganTransaction = false;
		try {

			beganTransaction = TransactionUtil.begin();

			City sellerCity = findCity(delegator, null, shipment.getSellerCity().getCityName(), true, false);
			City buyerCity = findCity(delegator, null, shipment.getBuyerCity().getCityName(), true, false);

			List<String> facilities = computeRoute(delegator, sellerCity.getFacility().getFacilityId(), buyerCity.getFacility().getFacilityId());

			GenericValue shipmentGenericValue = delegator.makeValue(Shipment.TABLE_NAME);
			shipmentGenericValue.set(Shipment.SHIPMENT_ID, delegator.getNextSeqId(Shipment.TABLE_NAME));
			shipmentGenericValue.set(Shipment.SELLER_NAME, shipment.getSellerName());
			shipmentGenericValue.set(Shipment.SELLER_PHONE, shipment.getSellerPhone());
			shipmentGenericValue.set(Shipment.SELLER_CITY_ID, sellerCity.getCityId());
			shipmentGenericValue.set(Shipment.BUYER_NAME, shipment.getBuyerName());
			shipmentGenericValue.set(Shipment.BUYER_PHONE, shipment.getBuyerPhone());
			shipmentGenericValue.set(Shipment.BUYER_CITY_ID, buyerCity.getCityId());
			shipmentGenericValue.set(Shipment.BUYER_FACILITY_ID, buyerCity.getFacility().getFacilityId());
			shipmentGenericValue.set(Shipment.DESCRIPTION, shipment.getDescription());
			shipmentGenericValue.set(Shipment.CREATION_DATE, UtilDateTime.nowTimestamp());
			shipmentGenericValue.set(Shipment.SHIPMENT_PATH, ShippingUtils.createPathAsString(facilities));
			shipmentGenericValue.set(Shipment.AWB, ShippingUtils.generateUniqeNumber());
			shipmentGenericValue.set(Shipment.SHIPMENT_STATUS_ID,
					findShipmentStatus(delegator, null, ShipmentStatus.CREATED, false).getShipmentStatusId());

			GenericValue shipmentGenericValueResult = create(delegator, shipmentGenericValue);
			Object shipmentId = shipmentGenericValueResult.get(Shipment.SHIPMENT_ID);
			addShipmentRoute(delegator, currentUserName, facilities, shipmentId);

			TransactionUtil.commit(beganTransaction);
			return String.valueOf(shipmentId);
		} catch (Exception e) {
			Debug.logError("Failure in create shipment", module);
			try {
				TransactionUtil.rollback(beganTransaction, "Failure in create shipment", e);
			} catch (GenericTransactionException e1) {
				throw new ShippingException(e1);
			}
			throw new ShippingException(e);
		}

	}

	public List<String> computeRoute(Delegator delegator, String sourceFacilityId, String targetFacilityId) throws ShippingException {
		RouteCache routeCache = isRouteExistInCache(delegator, sourceFacilityId, targetFacilityId);
		List<String> facilities;
		if (routeCache == null) {
			List<FacilityMap> facilityMap = getFacilityMap(delegator, true);
			facilities = ShippingUtils.computeRoute(facilityMap, sourceFacilityId, targetFacilityId);

			// Cache route in database
			// execute in different thread(when create new shipment)
			Runnable r = new Runnable() {
				public void run() {
					addRouteInCacheTable(delegator, sourceFacilityId, targetFacilityId, facilities);
				}
			};
			new Thread(r).start();
		} else {
			String route = routeCache.getRoute();
			facilities = ShippingUtils.splitPath(route);
		}
		return facilities;
	}

	private void addRouteInCacheTable(Delegator delegator, String sourceFacilityId, String targetFacilityId, List<String> facilities) {
		try {
			TransactionUtil.doNewTransaction(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					GenericValue routeCache = delegator.makeValue(RouteCache.TABLE_NAME);
					routeCache.set(RouteCache.SOURCE_FACILITY_ID, sourceFacilityId);
					routeCache.set(RouteCache.TARGET_FACILITY_ID, targetFacilityId);
					routeCache.set(RouteCache.ROUTE, ShippingUtils.createPathAsString(facilities));
					create(delegator, routeCache);
					return null;
				}
			}, "errorAddCache", 0, true);
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
		}
	}

	private void addShipmentRoute(Delegator delegator, GenericValue currentUserName, List<String> facilities, Object shipmentId)
			throws ShippingException {
		List<GenericValue> storeAll = new LinkedList<GenericValue>();
		String createdStatusId = findShipmentStatus(delegator, null, ShipmentStatus.CREATED, false).getShipmentStatusId();
		String pickedStatusId = findShipmentStatus(delegator, null, ShipmentStatus.PICKED, false).getShipmentStatusId();
		String movedStatusId = findShipmentStatus(delegator, null, ShipmentStatus.MOVED, false).getShipmentStatusId();
		String deliverdStatusId = findShipmentStatus(delegator, null, ShipmentStatus.DELIVERED, false).getShipmentStatusId();

		GenericValue createdRoute = delegator.makeValue(ShipmentRoute.TABLE_NAME);
		createdRoute.set(ShipmentRoute.SHIPMENT_ROUTE_ID, delegator.getNextSeqId(ShipmentRoute.TABLE_NAME));
		createdRoute.set(ShipmentRoute.USER_NAME, currentUserName.getString("userLoginId"));

		createdRoute.set(ShipmentRoute.SHIPMENT_STATUS_ID, createdStatusId);
		createdRoute.set(ShipmentRoute.SHIPMENT_ID, shipmentId);
		createdRoute.set(ShipmentRoute.ACTION_DATE, UtilDateTime.nowTimestamp());
		storeAll.add(createdRoute);

		for (int i = 0; i < facilities.size(); i++) {
			String facility = facilities.get(i);
			GenericValue pickedAnMoved = delegator.makeValue(ShipmentRoute.TABLE_NAME);
			pickedAnMoved.set(ShipmentRoute.SHIPMENT_ROUTE_ID, delegator.getNextSeqId(ShipmentRoute.TABLE_NAME));
			pickedAnMoved.set(ShipmentRoute.SHIPMENT_ID, shipmentId);
			pickedAnMoved.set(ShipmentRoute.DESTINATION_FACILITY_ID, facility);
			if (i == 0) {
				pickedAnMoved.set(ShipmentRoute.SHIPMENT_STATUS_ID, pickedStatusId);
			} else {
				pickedAnMoved.set(ShipmentRoute.SHIPMENT_STATUS_ID, movedStatusId);
			}
			storeAll.add(pickedAnMoved);
		}

		GenericValue deilverd = delegator.makeValue(ShipmentRoute.TABLE_NAME);
		deilverd.set(ShipmentRoute.SHIPMENT_ROUTE_ID, delegator.getNextSeqId(ShipmentRoute.TABLE_NAME));
		deilverd.set(ShipmentRoute.SHIPMENT_ID, shipmentId);
		deilverd.set(ShipmentRoute.SHIPMENT_STATUS_ID, deliverdStatusId);
		storeAll.add(deilverd);
		storeAll(delegator, storeAll);
	}

	public Shipment findShipment(Delegator delegator, String shipmentId, boolean deepFetch, boolean useCache) throws ShippingException {
		if (StringUtils.isEmpty(shipmentId)) {
			throw new ShippingException("Shipment ID is NULL");
		}
		EntityQuery entityQuery = EntityQuery.use(delegator).from(Shipment.TABLE_NAME);
		entityQuery.where(Shipment.SHIPMENT_ID, shipmentId);

		GenericValue genericValue = queryOne(entityQuery, useCache);
		if (UtilValidate.isNotEmpty(genericValue)) {
			Shipment shipment = ShippingMapper.toShipment(genericValue, true);
			if (deepFetch) {
				GenericValue shipmentStatus = getRelatedOne(genericValue, Shipment.REL_SHIPMENT_STATUS, useCache);
				shipment.setShipmentStatus(ShippingMapper.toShipmentStatus(shipmentStatus, true));

				GenericValue currentFacility = getRelatedOne(genericValue, Shipment.REL_CURRENT_FACILITY, useCache);
				// When create and deliver shipment the facility be null
				if (currentFacility != null) {
					shipment.setCurrentFacility(ShippingMapper.toFacility(currentFacility, false));
				}

				GenericValue sellerCity = getRelatedOne(genericValue, Shipment.REL_SELLER_CITY, useCache);
				shipment.setSellerCity(ShippingMapper.toCity(sellerCity, true));

				GenericValue buyerCity = getRelatedOne(genericValue, Shipment.REL_BUYER_CITY, useCache);
				shipment.setBuyerCity(ShippingMapper.toCity(buyerCity, true));

				GenericValue buyerFacility = getRelatedOne(genericValue, Shipment.REL_BUYER_FACILITY, useCache);
				shipment.setBuyerFacility(ShippingMapper.toFacility(buyerFacility, true));

			}
			return shipment;
		} else {
			throw new ShippingException("Cannot retrieve shipment ID= " + shipmentId);
		}
	}

	public String retrieveShipmentRoute(Delegator delegator, String shipmentId) throws ShippingException {
		if (StringUtils.isEmpty(shipmentId)) {
			throw new ShippingException("Shipment ID is NULL");
		}
		EntityQuery entityQuery = EntityQuery.use(delegator).select(Shipment.SHIPMENT_PATH).from(Shipment.TABLE_NAME);
		entityQuery.where(Shipment.SHIPMENT_ID, shipmentId);

		GenericValue genericValue = queryOne(entityQuery, false);
		if (UtilValidate.isNotEmpty(genericValue)) {
			return genericValue.getString(Shipment.SHIPMENT_PATH);
		} else {
			throw new ShippingException("Cannot retrieve shipment ID= " + shipmentId);
		}
	}

	// helper method if any exception occur her should't throw exception
	private RouteCache isRouteExistInCache(Delegator delegator, String sourceFacilityId, String targetFacilityId) {
		try {
			if (StringUtils.isEmpty(sourceFacilityId)) {
				return null;
			}
			if (StringUtils.isEmpty(targetFacilityId)) {
				return null;
			}
			EntityQuery entityQuery = EntityQuery.use(delegator).from(RouteCache.TABLE_NAME).where(RouteCache.SOURCE_FACILITY_ID, sourceFacilityId,
					RouteCache.TARGET_FACILITY_ID, targetFacilityId);
			GenericValue genericValue = queryOne(entityQuery, false);
			if (genericValue == null) {
				return null;
			}
			RouteCache routeCache = ShippingMapper.toRouteCache(genericValue, false);
			GenericValue sourceGeneric = getRelatedOne(genericValue, RouteCache.REL_SOURCE_FACILITY, false);
			routeCache.setSourceFacility(ShippingMapper.toFacility(sourceGeneric, false));

			GenericValue targetGeneric = getRelatedOne(genericValue, RouteCache.REL_TARGET_FACILITY, false);
			routeCache.setTargetFacility(ShippingMapper.toFacility(targetGeneric, false));
			return routeCache;
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return null;
		}

	}

	public List<ShipmentRoute> findShipmentRoute(Delegator delegator, String shipmentId) throws ShippingException {
		if (StringUtils.isEmpty(shipmentId)) {
			throw new ShippingException("Shipment ID is NULL");
		}
		EntityQuery entityQuery = EntityQuery.use(delegator).from(ShipmentRoute.TABLE_NAME);
		List<EntityCondition> exprsAnd = new LinkedList<EntityCondition>();
		exprsAnd.add(EntityCondition.makeCondition(ShipmentRoute.SHIPMENT_ID, EntityOperator.EQUALS, shipmentId));
		exprsAnd.add(EntityCondition.makeCondition(ShipmentRoute.ACTION_DATE, EntityOperator.NOT_EQUAL, null));
		exprsAnd.add(EntityCondition.makeCondition(ShipmentRoute.USER_NAME, EntityOperator.NOT_EQUAL, null));
		entityQuery.where(exprsAnd);

		List<GenericValue> genericValues = queryList(entityQuery, false);
		List<ShipmentRoute> ShipmentRoutes = new ArrayList<ShipmentRoute>();
		for (GenericValue ShipmentRouteGenericValue : genericValues) {
			ShipmentRoute shipmentRoute = ShippingMapper.toShipmentRoute(ShipmentRouteGenericValue, true);
			GenericValue shipmentStatus = getRelatedOne(ShipmentRouteGenericValue, ShipmentStatus.TABLE_NAME, false);
			shipmentRoute.setShipmentStatus(ShippingMapper.toShipmentStatus(shipmentStatus, true));

			GenericValue toFacility = getRelatedOne(ShipmentRouteGenericValue, Facility.TABLE_NAME, false);
			if (toFacility != null) {
				shipmentRoute.setDestinationFacilityId(ShippingMapper.toFacility(toFacility, false));
			}
			ShipmentRoutes.add(shipmentRoute);
		}

		return ShipmentRoutes;
	}

	public ShipmentStatus findShipmentStatus(Delegator delegator, String shipmentStatusId, String statusName, boolean useCache)
			throws ShippingException {
		EntityQuery entityQuery = EntityQuery.use(delegator).from(ShipmentStatus.TABLE_NAME);
		if (StringUtils.isNotEmpty(shipmentStatusId)) {
			entityQuery.where(ShipmentStatus.SHIPMENT_STATUS_ID, shipmentStatusId);
		} else if (StringUtils.isNotEmpty(statusName)) {
			entityQuery.where(ShipmentStatus.STATUS_NAME, statusName);
		} else {
			throw new ShippingException("All parameters is empty");
		}
		GenericValue genericValue = queryOne(entityQuery, useCache);
		ShipmentStatus shipmentStatus = ShippingMapper.toShipmentStatus(genericValue, true);
		return shipmentStatus;

	}

	public Facility findFacility(Delegator delegator, String facilityId, String facilityName, boolean useCache) throws ShippingException {
		EntityQuery entityQuery = EntityQuery.use(delegator).from(Facility.TABLE_NAME);
		if (StringUtils.isNotEmpty(facilityId)) {
			entityQuery.where(Facility.FACILITY_ID, facilityId);
		} else if (StringUtils.isNotEmpty(facilityName)) {
			entityQuery.where(Facility.FACILITY_NAME, facilityName);
		} else {
			throw new ShippingException("All parameters is empty");
		}
		GenericValue genericValue = queryOne(entityQuery, useCache);
		Facility facility = ShippingMapper.toFacility(genericValue, true);
		return facility;

	}

	public City findCity(Delegator delegator, String cityId, String cityName, boolean withFacility, boolean useCache) throws ShippingException {
		EntityQuery cityQuery = EntityQuery.use(delegator).from(City.TABLE_NAME);
		if (StringUtils.isNotEmpty(cityId)) {
			cityQuery.where(City.CITY_ID, cityId);
		} else if (StringUtils.isNotEmpty(cityName)) {
			cityQuery.where(City.CITY_NAME, cityName);
		} else {
			throw new ShippingException("All parameters is empty");
		}

		GenericValue genericValue = queryOne(cityQuery, useCache);
		City city = ShippingMapper.toCity(genericValue, true);
		if (withFacility) {
			GenericValue facilityGenericValue = getRelatedOne(genericValue, Facility.TABLE_NAME, useCache);
			city.setFacility(ShippingMapper.toFacility(facilityGenericValue, true));
		}
		return city;

	}

	public String convertPathToFacilityName(Delegator delegator, String shipmentPath) throws ShippingException {
		List<String> shipmentPathList = ShippingUtils.splitPath(shipmentPath);
		StringBuilder stringBuilder = new StringBuilder();
		int size = shipmentPathList.size();
		for (int i = 0; i < size; i++) {
			stringBuilder.append(findFacility(delegator, shipmentPathList.get(i), null, false).getFacilityName());
			if (size - 1 != i) {
				stringBuilder.append(" --> ");
			}
		}
		return stringBuilder.toString();
	}

	public void pickShipment(Delegator delegator, GenericValue userLogin, String shipmentId) throws ShippingException {
		boolean okay = true;
		boolean rolledBack = true;
		boolean beganTransaction = false;
		try {
			beganTransaction = TransactionUtil.begin();
			Shipment shipment = findShipment(delegator, shipmentId, true, false);
			List<String> facilitiesId = ShippingUtils.splitPath(shipment.getShipmentPath());
			if (CollectionUtils.isNotEmpty(facilitiesId)) {

				String destinationFacilityId = facilitiesId.get(0);
				EntityQuery entityQuery = EntityQuery.use(delegator).from(Shipment.TABLE_NAME).where(Shipment.SHIPMENT_ID, shipmentId);
				GenericValue genericValue = queryOne(entityQuery, false);
				String pickedStatusId = findShipmentStatus(delegator, null, ShipmentStatus.PICKED, true).getShipmentStatusId();
				genericValue.set(Shipment.SHIPMENT_STATUS_ID, pickedStatusId);
				genericValue.set(Shipment.CURRENT_FACILITY_ID, destinationFacilityId);
				store(delegator, genericValue);
				updateShipmentRoute(delegator, userLogin, shipmentId, destinationFacilityId, pickedStatusId);
			}

		} catch (Exception e) {
			okay = false;
			Debug.logError(e, e.getLocalizedMessage(), module);
			try {
				TransactionUtil.rollback(beganTransaction, e.getLocalizedMessage(), e);
				rolledBack = true;
			} catch (GenericTransactionException gte2) {
				Debug.logError(gte2, "Unable to rollback transaction", module);
				rolledBack = false;
			}
			throw new ShippingException(e.getLocalizedMessage());
		} finally {
			if (!okay) {
				if (!rolledBack) {
					try {
						TransactionUtil.rollback(beganTransaction, "Failure in processing iDEAL callback", null);
					} catch (GenericTransactionException gte) {
						Debug.logError(gte, "Unable to rollback transaction", module);
						throw new ShippingException(gte.getLocalizedMessage());
					}
				}
			} else {
				try {
					TransactionUtil.commit(beganTransaction);
				} catch (GenericTransactionException gte) {
					Debug.logError(gte, "Unable to commit transaction", module);
					throw new ShippingException(gte.getLocalizedMessage());
				}
			}
		}
	}

	public void moveShipment(Delegator delegator, GenericValue userLogin, String shipmentId) throws ShippingException {

		boolean okay = true;
		boolean rolledBack = true;
		boolean beganTransaction = false;
		try {
			beganTransaction = TransactionUtil.begin();

			Shipment shipment = findShipment(delegator, shipmentId, true, false);
			String movedStatusId = findShipmentStatus(delegator, null, ShipmentStatus.MOVED, true).getShipmentStatusId();
			String destinationFacilityId = "";
			List<String> facilitiesId = ShippingUtils.splitPath(shipment.getShipmentPath());
			if (CollectionUtils.isNotEmpty(facilitiesId)) {
				// get next facility ID -->
				for (int i = 0; i < facilitiesId.size(); i++) {
					String facility = facilitiesId.get(i);
					if (shipment.getCurrentFacility().getFacilityId().equals(facility)) {
						if (i != facilitiesId.size() - 1) {
							destinationFacilityId = facilitiesId.get(i + 1);
							break;
						}
					}
				}
			}
			EntityQuery entityQuery = EntityQuery.use(delegator).from(Shipment.TABLE_NAME).where(Shipment.SHIPMENT_ID, shipmentId);
			GenericValue genericValue = queryOne(entityQuery, false);
			if (!shipment.getCurrentFacility().getFacilityId().equals(movedStatusId)) {
				genericValue.set(Shipment.SHIPMENT_STATUS_ID, movedStatusId);
			}
			genericValue.set(Shipment.CURRENT_FACILITY_ID, destinationFacilityId);

			store(delegator, genericValue);
			updateShipmentRoute(delegator, userLogin, shipmentId, destinationFacilityId, movedStatusId);

		} catch (Exception e) {
			okay = false;
			Debug.logError(e, e.getLocalizedMessage(), module);
			try {
				TransactionUtil.rollback(beganTransaction, e.getLocalizedMessage(), e);
				rolledBack = true;
			} catch (GenericTransactionException gte2) {
				Debug.logError(gte2, "Unable to rollback transaction", module);
				rolledBack = false;
			}
			throw new ShippingException(e.getLocalizedMessage());
		} finally {
			if (!okay) {
				if (!rolledBack) {
					try {
						TransactionUtil.rollback(beganTransaction, "Failure in processing iDEAL callback", null);
					} catch (GenericTransactionException gte) {
						Debug.logError(gte, "Unable to rollback transaction", module);
						throw new ShippingException(gte.getLocalizedMessage());
					}
				}
			} else {
				try {
					TransactionUtil.commit(beganTransaction);
				} catch (GenericTransactionException gte) {
					Debug.logError(gte, "Unable to commit transaction", module);
					throw new ShippingException(gte.getLocalizedMessage());
				}
			}
		}
	}

	public void deilverShipment(Delegator delegator, GenericValue userLogin, String shipmentId) throws ShippingException {
		boolean okay = true;
		boolean rolledBack = true;
		boolean beganTransaction = false;
		try {
			beganTransaction = TransactionUtil.begin();
			EntityQuery entityQuery = EntityQuery.use(delegator).from(Shipment.TABLE_NAME).where(Shipment.SHIPMENT_ID, shipmentId);
			GenericValue genericValue = queryOne(entityQuery, false);
			String deliveredStatus = findShipmentStatus(delegator, null, ShipmentStatus.DELIVERED, true).getShipmentStatusId();
			genericValue.set(Shipment.SHIPMENT_STATUS_ID, deliveredStatus);
			genericValue.set(Shipment.CURRENT_FACILITY_ID, null);
			store(delegator, genericValue);
			GenericValue shipmentRoute = EntityQuery.use(delegator).from(ShipmentRoute.TABLE_NAME)
					.where(ShipmentRoute.SHIPMENT_ID, shipmentId, ShipmentRoute.SHIPMENT_STATUS_ID, deliveredStatus).queryOne();
			if (UtilValidate.isNotEmpty(shipmentRoute)) {
				shipmentRoute.set(ShipmentRoute.USER_NAME, userLogin.getString("userLoginId"));
				shipmentRoute.set(ShipmentRoute.ACTION_DATE, UtilDateTime.nowTimestamp());
				store(delegator, shipmentRoute);
			} else {
				throw new ShippingException("Cannot find shipment route");
			}

		} catch (Exception e) {
			okay = false;
			Debug.logError(e, e.getLocalizedMessage(), module);
			try {
				TransactionUtil.rollback(beganTransaction, e.getLocalizedMessage(), e);
				rolledBack = true;
			} catch (GenericTransactionException gte2) {
				Debug.logError(gte2, "Unable to rollback transaction", module);
				rolledBack = false;
			}
			throw new ShippingException(e.getLocalizedMessage());
		} finally {
			if (!okay) {
				if (!rolledBack) {
					try {
						TransactionUtil.rollback(beganTransaction, "Failure in processing iDEAL callback", null);
					} catch (GenericTransactionException gte) {
						Debug.logError(gte, "Unable to rollback transaction", module);
						throw new ShippingException(gte.getLocalizedMessage());
					}
				}
			} else {
				try {
					TransactionUtil.commit(beganTransaction);
				} catch (GenericTransactionException gte) {
					Debug.logError(gte, "Unable to commit transaction", module);
					throw new ShippingException(gte.getLocalizedMessage());
				}
			}
		}

	}

	private void updateShipmentRoute(Delegator delegator, GenericValue userLogin, String shipmentId, String destinationFacilityId,
			String shipmentStatusId) throws ShippingException {
		EntityQuery entityQuery = EntityQuery.use(delegator).from(ShipmentRoute.TABLE_NAME).where(ShipmentRoute.SHIPMENT_ID, shipmentId,
				ShipmentRoute.SHIPMENT_STATUS_ID, shipmentStatusId, ShipmentRoute.DESTINATION_FACILITY_ID, destinationFacilityId);
		GenericValue shipmentRoute = queryOne(entityQuery, false);
		if (UtilValidate.isNotEmpty(shipmentRoute)) {
			shipmentRoute.set(ShipmentRoute.USER_NAME, userLogin.getString("userLoginId"));
			shipmentRoute.set(ShipmentRoute.ACTION_DATE, UtilDateTime.nowTimestamp());
			store(delegator, shipmentRoute);
		} else {
			throw new ShippingException("Cannot find shpment route ");
		}
	}

	public List<FacilityMap> getFacilityMap(Delegator delegator, boolean deep) throws ShippingException {
		List<FacilityMap> list = new ArrayList<FacilityMap>();
		EntityQuery entityQuery = EntityQuery.use(delegator).from(FacilityMap.TABLE_NAME).orderBy(FacilityMap.CENTRAL_FACILITY_ID);
		List<GenericValue> genericsValue = queryList(entityQuery, true);
		if (UtilValidate.isNotEmpty(genericsValue)) {
			for (GenericValue genericValue : genericsValue) {
				FacilityMap facilityMap = new FacilityMap();
				if (deep) {
					Facility centralFacility = ShippingMapper.toFacility(getRelatedOne(genericValue, FacilityMap.REL_CENTRAL_FACILITY, false), true);
					Facility connectedFacility = ShippingMapper.toFacility(getRelatedOne(genericValue, FacilityMap.REL_CONNECTED_FACILITY, false),
							true);
					facilityMap.setCentralFacility(centralFacility);
					facilityMap.setConnectedFacility(connectedFacility);
				} else {
					Facility centralFacility = new Facility(genericValue.getString(FacilityMap.CENTRAL_FACILITY_ID), StringUtils.EMPTY);
					Facility connectedFacility = new Facility(genericValue.getString(FacilityMap.CONNECTED_FACILITY_ID), StringUtils.EMPTY);
					facilityMap.setCentralFacility(centralFacility);
					facilityMap.setConnectedFacility(connectedFacility);
				}
				list.add(facilityMap);
			}
			return list;
		} else {
			throw new ShippingException("Cannot get shipment route");
		}
	}

	private GenericValue getRelatedOne(GenericValue genericValue, String relationName, boolean useCache) throws ShippingException {
		try {
			return genericValue.getRelatedOne(relationName, useCache);
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}
	}

	private GenericValue queryOne(EntityQuery entityQuery, boolean useCache) throws ShippingException {
		try {
			return entityQuery.cache(useCache).queryOne();
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}
	}

	private GenericValue create(Delegator delegator, GenericValue genericValue) throws ShippingException {
		try {
			return delegator.create(genericValue);
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}
	}

	private int storeAll(Delegator delegator, List<GenericValue> list) throws ShippingException {
		try {
			return delegator.storeAll(list);
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}
	}

	private List<GenericValue> queryList(EntityQuery entityQuery, boolean useCache) throws ShippingException {
		try {
			return entityQuery.cache(useCache).queryList();
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}
	}

	private int store(Delegator delegator, GenericValue genericValue) throws ShippingException {
		try {
			return delegator.store(genericValue);
		} catch (GenericEntityException e) {
			Debug.logError(e, e.getLocalizedMessage(), module);
			throw new ShippingException(e.getLocalizedMessage(), e.fillInStackTrace());
		}

	}
}
