package org.apache.ofbiz.souq.shipping.model;

import java.sql.Timestamp;

public class ShipmentRoute {

	public static final String TABLE_NAME = "SouqShipmentRoute";
	public static final String SHIPMENT_ROUTE_ID = "shipmentRouteId";
	public static final String ACTION_DATE = "actionDate";
	public static final String USER_NAME = "userName";
	public static final String SHIPMENT_STATUS_ID = "shipmentStatusId";
	public static final String SHIPMENT_ID = "shipmentId";
	public static final String DESTINATION_FACILITY_ID = "destinationFacilityId";

	private String shipmentRouteId;
	private Timestamp actionDate;
	private String userName;
	private ShipmentStatus shipmentStatus;
	private Shipment shipmentId;
	private Facility destinationFacilityId;

	/**
	 * @return the shipmentRouteId
	 */
	public String getShipmentRouteId() {
		return shipmentRouteId;
	}

	/**
	 * @param shipmentRouteId
	 *            the shipmentRouteId to set
	 */
	public void setShipmentRouteId(String shipmentRouteId) {
		this.shipmentRouteId = shipmentRouteId;
	}

	/**
	 * @return the actionDate
	 */
	public Timestamp getActionDate() {
		return actionDate;
	}

	/**
	 * @param actionDate
	 *            the actionDate to set
	 */
	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the shipmentStatus
	 */
	public ShipmentStatus getShipmentStatus() {
		return shipmentStatus;
	}

	/**
	 * @param shipmentStatus
	 *            the shipmentStatus to set
	 */
	public void setShipmentStatus(ShipmentStatus shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

	/**
	 * @return the shipmentId
	 */
	public Shipment getShipmentId() {
		return shipmentId;
	}

	/**
	 * @param shipmentId
	 *            the shipmentId to set
	 */
	public void setShipmentId(Shipment shipmentId) {
		this.shipmentId = shipmentId;
	}

	/**
	 * @return the destinationFacilityId
	 */
	public Facility getDestinationFacilityId() {
		return destinationFacilityId;
	}

	/**
	 * @param destinationFacilityId
	 *            the destinationFacilityId to set
	 */
	public void setDestinationFacilityId(Facility destinationFacilityId) {
		this.destinationFacilityId = destinationFacilityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShipmentRoute [shipmentRouteId=" + shipmentRouteId + ", actionDate=" + actionDate + ", userName=" + userName + ", shipmentStatus="
				+ shipmentStatus + ", shipmentId=" + shipmentId + ", destinationFacilityId=" + destinationFacilityId + "]";
	}

}
