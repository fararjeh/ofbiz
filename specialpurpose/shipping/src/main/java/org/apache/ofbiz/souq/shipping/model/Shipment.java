package org.apache.ofbiz.souq.shipping.model;

import java.sql.Timestamp;

public class Shipment {

	public static final String TABLE_NAME = "SouqShipment";

	public static final String REL_CURRENT_FACILITY = "currentFacility" + Facility.TABLE_NAME;
	public static final String REL_SHIPMENT_STATUS = "shipmentStatus" + ShipmentStatus.TABLE_NAME;
	public static final String REL_SELLER_CITY = "sellerCity" + City.TABLE_NAME;
	public static final String REL_BUYER_CITY = "buyerCity" + City.TABLE_NAME;
	public static final String REL_BUYER_FACILITY = "buyerFacility" + Facility.TABLE_NAME;

	public static final String SHIPMENT_ID = "shipmentId";
	public static final String SELLER_NAME = "sellerName";
	public static final String SELLER_PHONE = "sellerPhone";
	public static final String SELLER_CITY_ID = "sellerCityId";
	public static final String BUYER_NAME = "buyerName";
	public static final String BUYER_PHONE = "buyerPhone";
	public static final String BUYER_CITY_ID = "buyerCityId";
	public static final String BUYER_FACILITY_ID = "buyerFacilityId";
	public static final String DESCRIPTION = "description";
	public static final String CREATION_DATE = "creationDate";
	public static final String SHIPMENT_PATH = "shipmentPath";
	public static final String AWB = "awb";
	public static final String SHIPMENT_STATUS_ID = "shipmentStatusId";
	public static final String CURRENT_FACILITY_ID = "currentFacilityId";

	private String shipmentId;

	private String sellerName;
	private String sellerPhone;
	private City sellerCity;

	private String buyerName;
	private String buyerPhone;
	private City buyerCity;
	private Facility buyerFacility;

	private long awb;
	private String description;
	private String shipmentPath;
	private Timestamp creationDate;
	private ShipmentStatus shipmentStatus;
	private Facility currentFacility;

	/**
	 * @return the shipmentId
	 */
	public String getShipmentId() {
		return shipmentId;
	}

	/**
	 * @param shipmentId
	 *            the shipmentId to set
	 */
	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}

	/**
	 * @return the sellerName
	 */
	public String getSellerName() {
		return sellerName;
	}

	/**
	 * @param sellerName
	 *            the sellerName to set
	 */
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	/**
	 * @return the sellerPhone
	 */
	public String getSellerPhone() {
		return sellerPhone;
	}

	/**
	 * @param sellerPhone
	 *            the sellerPhone to set
	 */
	public void setSellerPhone(String sellerPhone) {
		this.sellerPhone = sellerPhone;
	}

	/**
	 * @return the sellerCity
	 */
	public City getSellerCity() {
		return sellerCity;
	}

	/**
	 * @param sellerCity
	 *            the sellerCity to set
	 */
	public void setSellerCity(City sellerCity) {
		this.sellerCity = sellerCity;
	}

	/**
	 * @return the buyerName
	 */
	public String getBuyerName() {
		return buyerName;
	}

	/**
	 * @param buyerName
	 *            the buyerName to set
	 */
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	/**
	 * @return the buyerPhone
	 */
	public String getBuyerPhone() {
		return buyerPhone;
	}

	/**
	 * @param buyerPhone
	 *            the buyerPhone to set
	 */
	public void setBuyerPhone(String buyerPhone) {
		this.buyerPhone = buyerPhone;
	}

	/**
	 * @return the buyerCity
	 */
	public City getBuyerCity() {
		return buyerCity;
	}

	/**
	 * @param buyerCity
	 *            the buyerCity to set
	 */
	public void setBuyerCity(City buyerCity) {
		this.buyerCity = buyerCity;
	}

	/**
	 * @return the buyerFacility
	 */
	public Facility getBuyerFacility() {
		return buyerFacility;
	}

	/**
	 * @param buyerFacility
	 *            the buyerFacility to set
	 */
	public void setBuyerFacility(Facility buyerFacility) {
		this.buyerFacility = buyerFacility;
	}

	/**
	 * @return the awb
	 */
	public long getAwb() {
		return awb;
	}

	/**
	 * @param awb
	 *            the awb to set
	 */
	public void setAwb(long awb) {
		this.awb = awb;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the shipmentPath
	 */
	public String getShipmentPath() {
		return shipmentPath;
	}

	/**
	 * @param shipmentPath
	 *            the shipmentPath to set
	 */
	public void setShipmentPath(String shipmentPath) {
		this.shipmentPath = shipmentPath;
	}

	/**
	 * @return the creationDate
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            the creationDate to set
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return the shipmentStatus
	 */
	public ShipmentStatus getShipmentStatus() {
		return shipmentStatus;
	}

	/**
	 * @param shipmentStatus
	 *            the shipmentStatus to set
	 */
	public void setShipmentStatus(ShipmentStatus shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

	/**
	 * @return the currentFacility
	 */
	public Facility getCurrentFacility() {
		return currentFacility;
	}

	/**
	 * @param currentFacility
	 *            the currentFacility to set
	 */
	public void setCurrentFacility(Facility currentFacility) {
		this.currentFacility = currentFacility;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Shipment [shipmentId=" + shipmentId + ", sellerName=" + sellerName + ", sellerPhone=" + sellerPhone + ", sellerCity=" + sellerCity
				+ ", buyerName=" + buyerName + ", buyerPhone=" + buyerPhone + ", buyerCity=" + buyerCity + ", buyerFacility=" + buyerFacility
				+ ", awb=" + awb + ", description=" + description + ", shipmentPath=" + shipmentPath + ", creationDate=" + creationDate
				+ ", shipmentStatus=" + shipmentStatus + ", currentFacility=" + currentFacility + "]";
	}

}
