package org.apache.ofbiz.souq.shipping.model;

public class City {
	public static final String TABLE_NAME = "SouqCity";
	public static final String CITY_ID = "cityId";
	public static final String CITY_NAME = "cityName";
	public static final String FACILITY_ID = "facilityId";

	private String cityId;
	private String cityName;
	private Facility facilityId;

	/**
	 * @return the cityId
	 */
	public String getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 *            the cityName to set
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * @return the facilityId
	 */
	public Facility getFacility() {
		return facilityId;
	}

	/**
	 * @param facilityId
	 *            the facilityId to set
	 */
	public void setFacility(Facility facilityId) {
		this.facilityId = facilityId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "City [cityId=" + cityId + ", cityName=" + cityName + ", facilityId=" + facilityId + "]";
	}

}
