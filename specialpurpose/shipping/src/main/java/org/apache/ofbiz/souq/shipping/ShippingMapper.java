package org.apache.ofbiz.souq.shipping;

import java.util.Map;

import org.apache.ofbiz.base.util.UtilValidate;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.souq.shipping.model.City;
import org.apache.ofbiz.souq.shipping.model.Facility;
import org.apache.ofbiz.souq.shipping.model.RouteCache;
import org.apache.ofbiz.souq.shipping.model.Shipment;
import org.apache.ofbiz.souq.shipping.model.ShipmentRoute;
import org.apache.ofbiz.souq.shipping.model.ShipmentStatus;

public final class ShippingMapper {

	/**
	 * @param genericValue
	 *            The facility database instance
	 * @param throwExecption
	 *            when genericValue null throw exception
	 * @return A facility instance</code>
	 */
	public static Facility toFacility(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		checkGenericValueInstance(genericValue, throwExecption);
		Facility facility = new Facility();
		facility.setFacilityId(genericValue.getString(Facility.FACILITY_ID));
		facility.setFacilityName(genericValue.getString(Facility.FACILITY_NAME));
		return facility;
	}

	public static City toCity(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		checkGenericValueInstance(genericValue, throwExecption);
		City city = new City();
		city.setCityId(genericValue.getString(City.CITY_ID));
		city.setCityName(genericValue.getString(City.CITY_NAME));
		return city;
	}

	public static ShipmentStatus toShipmentStatus(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		checkGenericValueInstance(genericValue, throwExecption);
		ShipmentStatus shipmentStatus = new ShipmentStatus();
		shipmentStatus.setShipmentStatusId(genericValue.getString(ShipmentStatus.SHIPMENT_STATUS_ID));
		shipmentStatus.setStatusName(genericValue.getString(ShipmentStatus.STATUS_NAME));
		return shipmentStatus;
	}

	public static Shipment toShipment(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		checkGenericValueInstance(genericValue, throwExecption);
		Shipment shipment = new Shipment();
		shipment.setAwb(genericValue.getLong(Shipment.AWB));
		shipment.setShipmentId(genericValue.getString(Shipment.SHIPMENT_ID));
		shipment.setDescription(genericValue.getString(Shipment.DESCRIPTION));
		shipment.setShipmentPath(genericValue.getString(Shipment.SHIPMENT_PATH));
		shipment.setCreationDate(genericValue.getTimestamp(Shipment.CREATION_DATE));
		shipment.setShipmentStatus(null);
		shipment.setCurrentFacility(null);

		shipment.setSellerName(genericValue.getString(Shipment.SELLER_NAME));
		shipment.setSellerPhone(genericValue.getString(Shipment.SELLER_PHONE));
		shipment.setSellerCity(null);

		shipment.setBuyerName(genericValue.getString(Shipment.BUYER_NAME));
		shipment.setBuyerPhone(genericValue.getString(Shipment.BUYER_PHONE));
		shipment.setBuyerCity(null);
		shipment.setBuyerFacility(null);

		return shipment;
	}

	public static Shipment toShipment(Map<String, Object> context) {
		Shipment shipment = new Shipment();
		shipment.setDescription((String) context.get(ShippingConstants.PARAM_DESCRIPTION));

		shipment.setSellerName((String) context.get(ShippingConstants.PARAM_SELLER_NAME));
		shipment.setSellerPhone((String) context.get(ShippingConstants.PARAM_SELLER_PHONE));
		City sellerCity = new City();
		sellerCity.setCityName((String) context.get(ShippingConstants.PARAM_SELLER_CITY));
		shipment.setSellerCity(sellerCity);

		shipment.setBuyerName((String) context.get(ShippingConstants.PARAM_BUYER_NAME));
		shipment.setBuyerPhone((String) context.get(ShippingConstants.PARAM_BUYER_PHONE));
		City buyerCity = new City();
		buyerCity.setCityName((String) context.get(ShippingConstants.PARAM_BUYER_CITY));
		shipment.setBuyerCity(buyerCity);

		return shipment;
	}

	public static ShipmentRoute toShipmentRoute(GenericValue genericValue, boolean throwExecption) {
		ShipmentRoute shipmentRoute = new ShipmentRoute();
		shipmentRoute.setShipmentRouteId(genericValue.getString(ShipmentRoute.SHIPMENT_ROUTE_ID));
		shipmentRoute.setActionDate(genericValue.getTimestamp(ShipmentRoute.ACTION_DATE));
		shipmentRoute.setUserName(genericValue.getString(ShipmentRoute.USER_NAME));
		return shipmentRoute;
	}

	private static void checkGenericValueInstance(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		if (UtilValidate.isNotEmpty(genericValue)) {
			if (genericValue == null) {
				throw new ShippingException("The genericValue instance is null");
			}
		}

	}

	public static RouteCache toRouteCache(GenericValue genericValue, boolean throwExecption) throws ShippingException {
		RouteCache routeCache = new RouteCache();
		routeCache.setRoute(genericValue.getString(RouteCache.ROUTE));
		return routeCache;
	}

}
