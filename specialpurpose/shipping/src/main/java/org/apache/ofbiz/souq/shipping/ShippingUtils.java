package org.apache.ofbiz.souq.shipping;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.souq.shipping.model.FacilityMap;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm.SingleSourcePaths;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

public final class ShippingUtils {

	private static long lastGeneratedId;

	public synchronized static long generateUniqeNumber() throws ShippingException {
		long generatedId = System.currentTimeMillis();
		if (generatedId == lastGeneratedId) {
			try {
				TimeUnit.NANOSECONDS.sleep(1);
			} catch (InterruptedException e) {
				Debug.logError(e, ShippingUtils.class.getName());
				throw new ShippingException(e.getLocalizedMessage());
			}
			generatedId = System.currentTimeMillis();
		}
		lastGeneratedId = generatedId;
		return generatedId;
	}

	public static GenericValue getCurrentUser(Map<String, Object> context) throws ShippingException {
		GenericValue userLogin = (GenericValue) context.get("userLogin");
		if (userLogin == null) {
			return userLogin;
		}
		throw new ShippingException("Cannot retrieve current logged in user");
	}

	public static List<String> splitPath(String path) {
		String[] facilitiesId = path.split(",");
		return Arrays.asList(facilitiesId);
	}

	public static List<String> computeRoute(List<FacilityMap> facilityMap, String sourceFacility, String destinationFacility) {
		Graph<String, DefaultEdge> unDirectedGraph = prepareData(facilityMap);
		DijkstraShortestPath<String, DefaultEdge> dijkstraAlg = new DijkstraShortestPath<>(unDirectedGraph);
		SingleSourcePaths<String, DefaultEdge> source = dijkstraAlg.getPaths(sourceFacility);
		GraphPath<String, DefaultEdge> path = source.getPath(destinationFacility);
		List<String> result = path.getVertexList();
		// Collections.reverse(result);
		return result;
	}

	public static String createPathAsString(List<String> path) {
		StringBuilder stringBuilder = new StringBuilder();
		int size = path.size();
		for (int i = 0; i < size; i++) {
			stringBuilder.append(path.get(i));
			if (size - 1 != i) {
				stringBuilder.append(",");
			}
		}
		return stringBuilder.toString();
	}

	private static Graph<String, DefaultEdge> prepareData(List<FacilityMap> map) {
		if (null == map) {
			// throw
		}
		if (map.size() < 1) {
			// throw
		}
		Graph<String, DefaultEdge> directedGraph = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
		for (FacilityMap facilityMap : map) {
			directedGraph.addVertex(facilityMap.getCentralFacility().getFacilityId());
			directedGraph.addVertex(facilityMap.getConnectedFacility().getFacilityId());
			directedGraph.addEdge(facilityMap.getCentralFacility().getFacilityId(), facilityMap.getConnectedFacility().getFacilityId());
		}

		return directedGraph;
	}
}
