package org.apache.ofbiz.souq.shipping;

import java.util.List;

import org.apache.ofbiz.base.util.GeneralException;

public class ShippingException extends GeneralException{

	private static final long serialVersionUID = -5807531656264087643L;

	public ShippingException() {
		super();
	}

	public ShippingException(List<String> messages, Throwable nested) {
		super(messages, nested);
	}

	public ShippingException(List<String> messages) {
		super(messages);
	}

	public ShippingException(String msg, List<String> messages, Throwable nested) {
		super(msg, messages, nested);
	}

	public ShippingException(String msg, List<String> messages) {
		super(msg, messages);
	}

	public ShippingException(String msg, Throwable nested) {
		super(msg, nested);
	}

	public ShippingException(String msg) {
		super(msg);
	}

	public ShippingException(Throwable nested) {
		super(nested);
	}

}
