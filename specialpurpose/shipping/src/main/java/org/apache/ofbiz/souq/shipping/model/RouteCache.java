package org.apache.ofbiz.souq.shipping.model;

public class RouteCache {

	public static final String TABLE_NAME = "SouqRouteCache";

	public static final String REL_SOURCE_FACILITY = "sourceFacility" + Facility.TABLE_NAME;
	public static final String REL_TARGET_FACILITY = "targetFacility" + Facility.TABLE_NAME;

	public static final String ROUTE = "route";
	public static final String SOURCE_FACILITY_ID = "sourceFacilityId";
	public static final String TARGET_FACILITY_ID = "targetFacilityId";

	private String route;
	private Facility sourceFacility;
	private Facility targetFacility;

	/**
	 * @return the route
	 */
	public String getRoute() {
		return route;
	}

	/**
	 * @param route
	 *            the route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}

	/**
	 * @return the sourceFacility
	 */
	public Facility getSourceFacility() {
		return sourceFacility;
	}

	/**
	 * @param sourceFacility
	 *            the sourceFacility to set
	 */
	public void setSourceFacility(Facility sourceFacility) {
		this.sourceFacility = sourceFacility;
	}

	/**
	 * @return the targetFacility
	 */
	public Facility getTargetFacility() {
		return targetFacility;
	}

	/**
	 * @param targetFacility
	 *            the targetFacility to set
	 */
	public void setTargetFacility(Facility targetFacility) {
		this.targetFacility = targetFacility;
	}

}
