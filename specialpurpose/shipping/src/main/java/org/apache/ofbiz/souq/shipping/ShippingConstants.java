package org.apache.ofbiz.souq.shipping;

public interface ShippingConstants {

	public static final String PARAM_ERROR = "error";
	public static final String PARAM_SUCCESS = "success";

	public static final String PARAM_BUYER_NAME = "buyerName";
	public static final String PARAM_BUYER_PHONE = "buyerPhone";
	public static final String PARAM_BUYER_CITY = "buyerCity";

	public static final String PARAM_SELLER_NAME = "sellerName";
	public static final String PARAM_SELLER_PHONE = "sellerPhone";
	public static final String PARAM_SELLER_CITY = "sellerCity";

	public static final String PARAM_DESCRIPTION = "description";

	public static final String PARAM_DELEGATOR = "delegator";
	public static final String PARAM_SHIPMENT_ACTION = "shipmentAction";
	public static final String PARAM_SHIPMENT_ID = "shipmentId";
	public static final String PARAM_USER_LOGIN = "userLogin";
	public static final String PARAM_PICK = "pick";
	public static final String PARAM_MOVE = "move";
	public static final String PARAM_DELIVER = "deliver";
	public static final String PARAM_ROUTE = "routes";

	public static final String PARAM_SOURCE_FACILITY = "sourceFacilityId";
	public static final String PARAM_TARGET_FACILITY = "targetFacilityId";
}
