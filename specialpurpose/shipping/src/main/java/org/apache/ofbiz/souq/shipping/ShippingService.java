package org.apache.ofbiz.souq.shipping;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.ofbiz.base.util.Debug;
import org.apache.ofbiz.base.util.UtilHttp;
import org.apache.ofbiz.entity.Delegator;
import org.apache.ofbiz.entity.GenericValue;
import org.apache.ofbiz.service.DispatchContext;
import org.apache.ofbiz.service.ServiceUtil;
import org.apache.ofbiz.souq.shipping.model.Shipment;
import org.apache.ofbiz.souq.shipping.model.ShipmentRoute;

/**
 * Services for shipping system
 */

public class ShippingService {

	public static final String module = ShippingService.class.getName();

	/*
	 * Create new shipment
	 */
	public static Map<String, Object> createShipment(DispatchContext ctx, Map<String, Object> context) {
		try {
			Map<String, Object> result = ServiceUtil.returnSuccess();
			Delegator delegator = ctx.getDelegator();
			Shipment shipment = ShippingMapper.toShipment(context);
			ShippingRepository shipmentRepo = ShippingRepository.getInstance();
			String shipmentId = shipmentRepo.createShipment(delegator, shipment, (GenericValue) context.get(ShippingConstants.PARAM_USER_LOGIN));
			result.put(ShippingConstants.PARAM_SHIPMENT_ID, shipmentId);
			return result;
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return ServiceUtil.returnError(e.getMessage());
		}

	}

	public static Map<String, Object> getShipment(Delegator delegator, String shipmentId) {
		try {
			if (StringUtils.isEmpty(shipmentId)) {
				throw new ShippingException("Shipment ID is NULL");
			}
			Map<String, Object> result = ServiceUtil.returnSuccess();
			ShippingRepository shippingRepsitory = ShippingRepository.getInstance();
			Shipment shipment = shippingRepsitory.findShipment(delegator, shipmentId, true, false);
			result.put(Shipment.class.getSimpleName(), shipment);
			result.put(ShippingConstants.PARAM_ROUTE, shippingRepsitory.convertPathToFacilityName(delegator, shipment.getShipmentPath()));
			result.put(ShipmentRoute.class.getSimpleName(), shippingRepsitory.findShipmentRoute(delegator, shipmentId));
			return result;
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return ServiceUtil.returnError(e.getMessage());
		}
	}

	public static String processShipmentActions(HttpServletRequest request, HttpServletResponse response) {
		Delegator delegator = (Delegator) request.getAttribute(ShippingConstants.PARAM_DELEGATOR);
		Map<String, Object> parameters = UtilHttp.getParameterMap(request);
		String shipmentAction = String.valueOf(parameters.get(ShippingConstants.PARAM_SHIPMENT_ACTION));
		String shipmentId = String.valueOf(parameters.get(ShippingConstants.PARAM_SHIPMENT_ID));
		GenericValue userLogin = (GenericValue) request.getSession().getAttribute(ShippingConstants.PARAM_USER_LOGIN);
		try {
			if (ShippingConstants.PARAM_PICK.equals(shipmentAction)) {
				ShippingRepository.getInstance().pickShipment(delegator, userLogin, shipmentId);

			} else if (ShippingConstants.PARAM_MOVE.equals(shipmentAction)) {
				ShippingRepository.getInstance().moveShipment(delegator, userLogin, shipmentId);

			} else if (ShippingConstants.PARAM_DELIVER.equals(shipmentAction)) {
				ShippingRepository.getInstance().deilverShipment(delegator, userLogin, shipmentId);
			}
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return ShippingConstants.PARAM_ERROR;
		}
		return ShippingConstants.PARAM_SUCCESS;
	}

	public static Map<String, Object> computeRoute(DispatchContext ctx, Map<String, Object> context) {
		try {
			Delegator delegator = ctx.getDelegator();
			ShippingRepository shipmentRepo = ShippingRepository.getInstance();

			String sourceFacilityId = (String) context.get(ShippingConstants.PARAM_SOURCE_FACILITY);
			String targetFacilityId = (String) context.get(ShippingConstants.PARAM_TARGET_FACILITY);

			List<String> facilites = shipmentRepo.computeRoute(delegator, sourceFacilityId, targetFacilityId);
			Map<String, Object> result = ServiceUtil.returnSuccess();
			result.put(ShippingConstants.PARAM_ROUTE, shipmentRepo.convertPathToFacilityName(delegator, ShippingUtils.createPathAsString(facilites)));
			return result;
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return ServiceUtil.returnError(e.getMessage());
		}
	}

	public static Map<String, Object> assignRoute(DispatchContext ctx, Map<String, Object> context) {
		try {
			Delegator delegator = ctx.getDelegator();
			ShippingRepository shipmentRepo = ShippingRepository.getInstance();

			String shipmentId = (String) context.get(ShippingConstants.PARAM_SHIPMENT_ID);
			String route = shipmentRepo.retrieveShipmentRoute(delegator, shipmentId);

			Map<String, Object> result = ServiceUtil.returnSuccess();
			result.put(ShippingConstants.PARAM_ROUTE, shipmentRepo.convertPathToFacilityName(delegator, route));
			return result;
		} catch (ShippingException e) {
			Debug.logError(e, module);
			return ServiceUtil.returnError(e.getMessage());
		}
	}

}
