<#if result.get('Shipment')??>
	<#assign shipment = result.get('Shipment')>
<div class="screenlet">
    <div class="screenlet-title-bar">
        <ul>
            <li class="h3">${uiLabelMap.shipmentDetails}</li>
        </ul>
        <br class="clear"/>
    </div>
    <div class="screenlet-body">
        <table border="0" cellpadding="2" cellspacing="0" class="basic-table">
       	   <tr>
            <td width="20%" align="right" class="label">${uiLabelMap.awb}</td>
            <td width="80%">${shipment.awb}</td>            
          </tr>
          
          <tr>
            <td width="20%" align="right" class="label">${uiLabelMap.creationDate}</td>
            <td width="80%">${shipment.creationDate}</td>            
          </tr>
          
          <tr>
            <td width="20%" align="right" class="label">${uiLabelMap.currentFacilityId}</td>
            <#if shipment.currentFacility??>
            	<td width="80%">${shipment.currentFacility.facilityName}</td>
            </#if>
          </tr>
          
          <tr>
            <td width="20%" align="right" class="label">Seller</td>
            <td width="80%">${shipment.sellerName} - ${shipment.sellerPhone} - ${shipment.sellerCity.cityName}</td>
          </tr>
          <tr>
            <td width="20%" align="right" class="label">Buyer</td>
            <td width="80%">${shipment.buyerName} - ${shipment.buyerPhone} - ${shipment.buyerCity.cityName}</td>
          </tr>         
          
          <tr>
            <td width="20%" align="right" class="label">${uiLabelMap.description}</td>
            <td width="80%">${shipment.description}</td>            
          </tr>
          
          <tr>
            <td width="20%" align="right" class="label">Route</td>
            <td width="80%">${result.get('routes')}</td>            
          </tr> 
          
        </table>
    </div>
</div>
<div class="screenlet">
    <div class="screenlet-title-bar">
        <ul>
            <li class="h3">${uiLabelMap.history}</li>
        </ul>
        <br class="clear"/>
    </div>
    <div class="screenlet-body">
        <table border="0" cellpadding="2" cellspacing="0" class="basic-table">
            <#list result.get('ShipmentRoute') as shipmentRoute>
            	<tr>
            	 <#if shipmentRoute.destinationFacilityId ??>
					<td >${shipmentRoute.shipmentStatus.statusName}, to facility ${shipmentRoute.destinationFacilityId.facilityName}, by ${shipmentRoute.userName}, on ${shipmentRoute.actionDate} </td>
	            <#else>	            
            		<td >${shipmentRoute.shipmentStatus.statusName}, by ${shipmentRoute.userName}, on ${shipmentRoute.actionDate} </td>
            	</#if>
            	</tr>            
		    </#list> 
        </table>
    </div>
</div>
<#else>	            
	Cannot retrieve shipment	
</#if>