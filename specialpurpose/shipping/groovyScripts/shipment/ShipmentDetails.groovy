import org.apache.ofbiz.entity.condition.*

//converted to java 

shipmentId = parameters.shipmentId

if (!shipmentId) {
    shipmentId = request.getAttribute("shipmentId")
}
shipment = from("SouqShipment").where("shipmentId", shipmentId).queryOne()

if(shipment){
    context.sellerCityName = from("SouqCity").where("cityId", shipment.sellerCityId).queryOne().get("cityName")
	context.buyerCityName = from("SouqCity").where("cityId", shipment.buyerCityId).queryOne().get("cityName")
	if(shipment.currentFacilityId){
		context.currentFacilityName = from("SouqFacility").where("facilityId", shipment.currentFacilityId).queryOne().get("facilityName")
	}else{
	context.currentFacilityName = "Not in facility"
	}
	
	

}
context.shipmentId = shipmentId
context.shipment = shipment

